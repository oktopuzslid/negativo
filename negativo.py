from PIL import Image

foto = Image.open('./imagen/ejercicio.jpg')

datos = list(foto.getdata())

datos_invertidos = [(255 - datos[x][0], 255 - datos[x][1], 255 - datos[x][2]) for x in range(len(datos))]
imagen_invertida = Image.new('RGB', foto.size)

imagen_invertida.putdata(datos_invertidos)

imagen_invertida.save('./imagen/NEG-flores-color.jpg')


imagen_invertida.close()

foto.close()