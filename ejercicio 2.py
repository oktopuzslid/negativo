import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from PIL import Image

imagen = mpimg.imread('./imagen/guacamayos.jpg')
imagen_3channels = plt.imshow(imagen)
ch1 = imagen[:, :, 0]
ch2 = imagen[:, :, 1]
ch3 = imagen[:, :, 2]

print(imagen)
plt.imshow(ch1)
plt.show()
plt.imshow(ch2)
plt.show()
plt.imshow(ch3)
plt.show()
plt.imshow(imagen)
plt.show()